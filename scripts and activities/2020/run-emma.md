###### The Tor Project - UX Team - Q3Q42020 - Anticensorship
---
# Activity: Run Emma Network Test

### Suggested Time
5-15 Minutes

### Materials Needed
Computer, Internet connection, CLI
> If you have a VPN activated, please turn it OFF for this test

### Participants
Middle and advanced users with minium command-line interface skills
Security trainers, privacy advocates, people we're designing for

### Objective
 Collect outputs worldwide to measure the reachability of the Tor network globally. Emma is a lightweight censorship analyser tool that tests if the machine it's running on can use the Tor network. Emma does so by testing if it can fetch websites such as torproject.org, connect to Tor relays, connect to default bridges, and connect to directory authorities.

### How to do it
1. You need to have [Go](https://golang.org) installed to compile emma. If you don't have Go installed, you can download it from the [Go project's website](https://golang.org/dl/).
2. Then, clone the repository by running
    ```
    git clone https://gitlab.torproject.org/tpo/anti-censorship/emma.git
    ```
3.  Go to your newly-cloned repository and compile the tool by running:
	
    In Linux run
	```
	cd emma && make
	```
	In MacOS & Windows run
	```
	cd emma
	go build
	```
4. If everything worked, your emma binary will be in the `bin` directory.  The last step is executing the program.
5. Once the report is ready we are done \o/ Thank you!

### Report Findings

 - We are collecting outputs worldwide to measure the reachability of the Tor network globally. 
 -  For sharing the output .md file with us you can attach it in an email to [phw@torproject.org](mailto:phw@torproject.org).
 - **Remember to mention the country you ran the test from.**  
 - You can encrypt your [email using PGP](https://nymity.ch/openpgp_public_key.txt). Remember to attach your own key.

### Report Bugs
If you find any problem meanwhile running Emma, please [open an issue](https://gitlab.torproject.org/tpo/anti-censorship/emma/-/issues).
