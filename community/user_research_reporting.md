# User Research Reporting

A good research is a good listener. Please be present during this process and share with us your perception about the product you're testing.

### Scenario:
### Interviewer:
### Partner Organization: 
### Location & Date:


---

### User Feedback
Describe here what the user is saying out loud.

- 
-
-
-

### Interviewer Observations
Qualitative descriptions about how the testing is going.

-
-
-
-


### Pain Points
Pain points are anything that prevents users from accomplishing their goals or getting things done. You can list them here so we can improve this user flow.

-
-
-
-


### Action Items
We want to convert your report in real developer tasks. Use this section to list critical items to share with developers.

-
-
-
-

```TOR UX Team - v201909-1```