# Tor Personas 
 
Persona is a tool that represents the needs, thoughts, and goals of the target user. We created personas because they will help us to drive human-centered design processes.

As part of our global south travels during 2018 and 2019, we got the lucky chance to meet a lot of different Tor users: from activists to journalists, all of them with different motivations, but demanding a usable private and secure tool to access the internet.

The Community and UX teams have been working collecting and mapping real user stories and finding patterns across them. It is how our Personas emerged from our in field research.


#### Process

* https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity/Notes/UserStories
* https://trac.torproject.org/projects/tor/ticket/30430
* https://lists.torproject.org/pipermail/ux/2019-May/000449.html